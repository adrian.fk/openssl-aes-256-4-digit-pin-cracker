#!/bin/bash
for i in {1000..9999}; do
  if openssl enc -aes-256-cbc -d -in "kryptert.dta" -out "dekryptert.txt" 2>/dev/null -k $i &&
    file dekryptert.txt | grep 'ISO-8859'
  then 
  	echo "PIN: "$i;
  	break;
  fi
done